@extends('email.base')
<?php
use App\Infrastructure\Lang\Translator;
$setNewPassword = Translator::translate('Задайте новый пароль для восстановления доступа к личному кабинету.');
$changePassword = Translator::translate('Сменить пароль');
?>
@section('content')
    <table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0;" width="100%">
        <tr>
            <td align="center">
                <br>
                @include('email.blocks.text', [
                    'text' => $setNewPassword
                ])
            </td>
        </tr>
        <tr>
            <td align="center">
                <br>
                @include('email.blocks.img-button', [
                    'href' => $resetPasswordLink,
                    'text' => $changePassword,
                    'src' => '',
                ])
                <br>
            </td>
        </tr>
    </table>


    <?=Translator::translate('если кнопка не работает:') ?> <a href="<?= $resetPasswordLink ?>" ><?=Translator::translate('ссылка') ?></a>
@endsection
