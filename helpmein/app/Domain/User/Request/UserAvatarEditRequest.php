<?php

declare(strict_types=1);

namespace App\Domain\User\Request;

use App\Domain\User\Model\User;
use Illuminate\Foundation\Http\FormRequest;

class UserAvatarEditRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'image' => 'sometimes|nullable|mimes:jpg,jpeg,bmp,png|file|max:2048',
        ];
    }

    public function attributes()
    {
        return [
            'avatar' => __('avatar')
        ];
    }
}
