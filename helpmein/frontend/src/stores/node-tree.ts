import {ref} from "vue";
import {defineStore} from "pinia";
import ApiService from "@/core/services/ApiService";

export const useNodeStore = defineStore("node", () => {
    const selectedNode = ref("");
    const selectedUser = ref("");
    const taskCount = ref({});
    const currentUsingNode = ref({});
    const currentUsingToNode = ref({});
    function setNode(id) {
        selectedNode.value = id;
    }
    function setCurrentUsingNode(node) {
        currentUsingNode.value = node;
    }
    function setCurrentUsingToNode(node) {
        currentUsingToNode.value = node;
    }
    const assignTask = function(taskId, taskCategoryId) {
        ApiService.post('/admin/task/move-to-folder/' + taskId, {
            task_category_id: taskCategoryId
        }).then((response) => {
            // this.loadData();
        })
    }
    return {
        selectedNode,
        selectedUser,
        currentUsingNode,
        currentUsingToNode,
        assignTask,
        taskCount,
        setCurrentUsingNode,
        setCurrentUsingToNode,
        setNode
    };
});
