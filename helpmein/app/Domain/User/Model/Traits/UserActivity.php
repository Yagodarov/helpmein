<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Traits;

use App\Domain\Client\Model\Client;

/**
 * @property @this Client
 */
trait UserActivity
{
    public function toggleActive()
    {
        $userClient = $this->teachers()->first()->pivot;
        $userClient->active = !$userClient->active;
        $userClient->save();
    }
}
