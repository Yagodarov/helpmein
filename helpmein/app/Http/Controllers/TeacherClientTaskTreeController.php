<?php

namespace App\Http\Controllers;

use App\Domain\Client\Model\Client;
use App\Domain\Task\Model\Task;
use App\Domain\Task\Request\TaskListWithAssignmentRequest;
use App\Domain\Task\Request\TaskMassAssignRequest;
use App\Domain\Task\Resource\UserTaskTreeAssignedInfoResource;
use App\Domain\Task\Resource\UserTaskAllInfoResource;
use App\Domain\Task\Service\UserTaskService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class TeacherClientTaskTreeController extends Controller
{
    public function massAssign(TaskMassAssignRequest $request, UserTaskService $userTaskService): JsonResponse
    {
        $message = $userTaskService->massAssign($request);
        return $this->getSuccessResponse($message);
    }

    public function listWithoutAssign(Request $request): JsonResponse {
        /** @var LengthAwarePaginator $tasks */
        $tasks = Task::taskListAll($request);
        return $this->getListItemsResponse($tasks, UserTaskAllInfoResource::class, $request);
    }

    /** Загрузка всех задач со статусами */
    public function listAllWithAssign(TaskListWithAssignmentRequest $request): JsonResponse {
        /** @var LengthAwarePaginator $tasks */
        $tasksResult = Task::taskListAllClient($request);
        $result = $this->getListItemsResponse(
            $tasksResult['tasks'],
            UserTaskTreeAssignedInfoResource::class, $request,
            ['message' => $tasksResult['message']]
        );
        return $result;
    }

    public function delete(Request $request): JsonResponse {
        /** @var Client $client */
        $client = Client::query()->find($request->get('user_id'));
        $client->tasks()->detach($request->get('task_id'));
        return $this->getSuccessResponse([]);
    }
}
