@extends('email.base')
<?php
use App\Infrastructure\Lang\Translator;
$inivationTeacher = Translator::translate('Ваш преподаватель приглашает вас присоединиться к Платформе HelpMeIn.');
$inivationTeacherLink = Translator::translate('Перейдите по ссылке, чтобы задать пароль для вашего личного кабинета.');
$setPassword = Translator::translate('Задать пароль');
?>
@section('content')
    <table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0;" width="100%">
        <tr>
            <td align="center">
                <br>
                @include('email.blocks.text', [
                    'text' => $inivationTeacher
                ])
                @include('email.blocks.text', [
                    'text' => $inivationTeacherLink
                ])
            </td>
        </tr>
        <tr>
            <td align="center">
                <br>
                @include('email.blocks.img-button', [
                    'href' => $resetPasswordLink,
                    'text' => $setPassword,
                    'src' =>  '',
                ])
                <br>
            </td>
        </tr>
    </table>
@endsection
