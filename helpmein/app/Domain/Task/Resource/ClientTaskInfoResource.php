<?php

declare(strict_types=1);

namespace App\Domain\Task\Resource;

use App\Domain\Task\Model\Task;
use App\Enum\TaskType;
use App\Enum\UserTaskStatus;
use App\Infrastructure\Http\Resource\EnumResource;
use App\Infrastructure\Http\Resource\JsonResource;

class ClientTaskInfoResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Task $task */
        $task = $this->resource;
        $type = new EnumResource($task->type);
        $questions = $task->questions;
        $resultQuestions = [];
        if (isset($task->answer->status)) {
            $status = new EnumResource($task->answer->status);
        } else {
            $status = new EnumResource(new UserTaskStatus(UserTaskStatus::ASSIGNED));
        }
        foreach ($questions as $question) {
            $answers = [];
            if (isset($question['answers'])) {
                foreach ($question['answers'] as $qAnswer) {
                    $qAnswer['checkBoxValue'] = false;
                    $answers[] = $qAnswer;
                }
                $question['radioValue'] = null;
                $question['answers'] = $answers;
            }
            $resultQuestions[] = $question;
        }
        $answer = $task->answer()->where('user_task.user_id','=', auth('sanctum')->user()->id)->first();
        return [
            'id' => $task->id,
            'name' => $task->name,
            'status' => $status,
            'answer' => $answer,
            'description' => $task->description,
            'task_category_id' => $task->task_category_id,
            'comment' => $task->comment,
            'comment_client' => $task->comment_client,
            'questions' => $resultQuestions,
            'difficult_level' => $task->difficult_level,
            'type' => $type,
        ];
    }
}
