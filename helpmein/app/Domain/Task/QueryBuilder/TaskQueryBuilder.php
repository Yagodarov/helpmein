<?php

namespace App\Domain\Task\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;

class TaskQueryBuilder extends Builder
{
    public function withUserTask($userId) {
        return $this->withWhereHas('answer', fn($query) =>
            $query->where('user_task.user_id','=',$userId)
        );
    }
}
