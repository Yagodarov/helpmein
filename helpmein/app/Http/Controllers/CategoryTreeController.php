<?php

namespace App\Http\Controllers;

use App\Domain\Task\Model\Task;
use App\Domain\TaskCategory\Model\TaskCategory;
use App\Domain\User\Model\User;
use Illuminate\Http\Request;

class CategoryTreeController extends Controller
{
    public function list(Request $request) {
        $tree = $this->queryList();
        $tree = $this->loadChildren($tree);
        return $tree->toArray();
    }

    public function queryList()
    {
        /** @var User $user */
        $user = auth('sanctum')->user();
        /** @var TaskCategory $categoryParent */
        $categoryParent = TaskCategory::query()
            ->whereNull('parent_id')
            ->where('user_id','=',$user->id)
            ->firstOrCreate([], [
                'name' => 'Коренная папка',
                'user_id' => auth('sanctum')->id()
            ]);
        $tree = TaskCategory::defaultOrder('asc')->descendantsAndSelf($categoryParent->id)->toTree()->first();
        $tree->setAttribute('count', Task::query()->where('task_category_id','=',$tree->id)->count());
        return $tree;
    }

    public function loadChildren($tree) {
        $childrens = $tree->children;
        foreach ($childrens as $children) {
            $this->loadChildren($children);
            $children->setAttribute('count', Task::query()->where('task_category_id','=',$children->id)->count());
        }
        return $tree;
    }

    public function loadChildrenWithCurrentClientTaskCount($tree) {
        $childrens = $tree->children;
        foreach ($childrens as $children) {
            $this->loadChildrenWithCurrentClientTaskCount($children);
            $task = Task::query()
                ->where('task_category_id','=',$children->id)
                ->withWhereHas('clients', function ($query) {
                    $query
                        ->where('user_task.user_id','=',\request()->get('selected_user'));
                })
                ->get();
            $count = $task->count();
            $children->setAttribute('count', $count);
        }
        return $tree;
    }

    public function listWithUser(Request $request) {
        $tree = $this->queryList();
        $tree = $this->loadChildrenWithCurrentClientTaskCount($tree);
        return $tree->toArray();
    }

    public function categoriesTaskCountList(Request $request) {
        $list = $this->listWithUser($request);
        $array = [];
        $traverse = function ($categoriesElement) use (&$traverse, &$array) {
            if (isset($categoriesElement['id'])) {
                $array[$categoriesElement['id']] = $categoriesElement['count'];
                $traverse($categoriesElement['children']);
            } else {
                foreach ($categoriesElement as $category) {
                    $traverse($category);
                }
            }
        };
        $traverse($list);
        return $this->getSuccessResponse($array);
    }

    public function add(Request $request) {
        /** @var User $user */
        $user = auth('sanctum')->user();
        /** @var TaskCategory $categoryParent */
        $categoryParent = TaskCategory::query()
            ->where('id','=',$request->get('id'))
            ->firstOrFail();
        $children = new TaskCategory([
            'name' => 'Новая папка',
            'user_id' => $user->id
        ]);
        $categoryParent->appendNode($children);
        return $children->toArray();
    }

    public function edit(Request $request) {
        /** @var User $user */
        $user = auth('sanctum')->user();
        /** @var TaskCategory $categoryParent */
        $categoryParent = TaskCategory::query()
            ->where('id','=',$request->get('id'))
            ->firstOrFail();
        $categoryParent->update([
            'name' => $request->get('name')
        ]);
        return $categoryParent->toArray();
    }

    public function replace(Request $request) {
        /** @var User $user */
        $user = auth('sanctum')->user();
        /** @var TaskCategory $categoryParent */
        $categoryFrom = TaskCategory::query()
            ->where('id','=',$request->get('from_id'))
            ->firstOrFail();
        $categoryTo = TaskCategory::query()
            ->where('id','=',$request->get('to_id'))
            ->firstOrFail();
        if ($request->get('move_to') == 'inner') {
            $categoryFrom->prependToNode($categoryTo)->save();
        } elseif ($request->get('move_to') == 'after') {
            $categoryFrom->afterNode($categoryTo)->save();
        } elseif ($request->get('move_to') == 'before') {
            $categoryFrom->beforeNode($categoryTo)->save();
        }
        return $this->getSuccessResponse([]);
    }

    public function delete(Request $request) {
        /** @var User $user */
        $user = auth('sanctum')->user();
        /** @var TaskCategory $categoryParent */
        $categoryParent = TaskCategory::query()
            ->where('id','=',$request->get('id'))
            ->firstOrFail()
            ->delete();
        return $this->getSuccessResponse([]);
    }
}
