<?php

return [
    'RoleType' => [
        'Teacher' => 'Teacher',
        'Client' => 'Client',
    ],
    'TaskType' => [
        'essay' => 'Эссе',
        'task' => 'Тест',
    ],
    'UserTaskStatus' => [
        'not_assigned' => 'Not Assigned',
        'assigned' => 'Assigned',
        'reassigned' => 'Reassigned',
        'in_review' => 'In review',
        'finished' => 'Finished',
    ]
];
