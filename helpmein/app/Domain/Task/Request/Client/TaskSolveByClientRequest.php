<?php

declare(strict_types=1);

namespace App\Domain\Task\Request\Client;

use App\Domain\User\Model\User;
use App\Enum\TaskType;
use App\Infrastructure\Lang\Translator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Fluent;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\RequiredIf;

class TaskSolveByClientRequest extends FormRequest
{
    public function rules(): array
    {
        /** @var User $user */
        return [
            'type' => 'sometimes',
            'answer.answer' => ['max:2048', new RequiredIf($this->type['id'] === TaskType::ESSAY)],
            'questions.*.answers.*.checkBoxValue' => [Rule::when(function(Fluent $input) {
                return $input->getAttributes()["type"] === "task";
            }, ['required', 'boolean'])],
        ];
    }

    public function attributes()
    {
        return [
            'answer.answer' => Translator::translate('answer')
        ];
    }
}
