Установка докер (только для пользователей Windows)
Для работы лучше использовать WSL, установить дистрибутив debian.
Инструкция: https://docs.microsoft.com/ru-ru/windows/wsl/install-win10 (ставим WSL 1)
После выполнения инструкции нужно:

Кликнуть правой кнопкой мышки на значке докера в трее, открыть настройки
Установить галочку Expose daemon on tcp://localhost:2375 without TLS

Открыть консоль WSL и выполнить команду echo "export DOCKER_HOST='tcp://localhost:2375'" >> ~/.bashrc

Проверить, что все работает командой docker -v

Разворачивание локально


Клонируем репозиторий, переходим в ветку dev


Переходим в ./helpmein


Копируем ./.env.example в ./.env


Переходим в ./helpmein/frontend


Копируем .env.example в .env

Устанавливаем npm версии 8.19.2 и выше
Выполняем команду npm install
Выполняем команду npm run build


Переходим в ./laradock


Копируем .env.example в .env

Выполняем команду make init для сборки и запуска контейнеров
Разворачивается долго!
Если инструмент make недоступен, открываем файл ./laradock/Makefile и выполняем инструкции из него вручную.


После этого можно открыть http://localhost/, где будет доступно фронт приложения.
Для доступа к апи приложения следует перейти по адресу http://localhost:8000/.
Если нужно поменять порты доступа фронту/api, то меняем значения переменных
NGINX_HOST_FRONTEND_PORT и NGINX_HOST_API_PORT в laradock/.env.
При изменении NGINX_HOST_API_PORT так же следует изменить FRONTEND_SERVER_API_BASE_URL,
где указан полный путь до сервера апи.


Подключаемся к контейнеру workspace make workspace-connect, выполняем команду php artisan key:generate --show

