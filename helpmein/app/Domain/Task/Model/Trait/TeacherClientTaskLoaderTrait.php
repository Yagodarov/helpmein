<?php
namespace App\Domain\Task\Model\Trait;

use App\Domain\Task\Model\Task;
use App\Infrastructure\Lang\Translator;
use Drandin\DeclensionNouns\Facades\DeclensionNoun;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Symfony\Component\HttpKernel\Exception\HttpException;

trait TeacherClientTaskLoaderTrait {
    /** Все задачи с назначениями и без */
    public static function taskListAllClient(Request $request): array
    {
        $message = '';
        $tasksWithoutFilter = QueryBuilder::for(Task::class)
            ->where('task_category_id', '=',$request->get('filter')['task_category_id'])
            ->orderBy('id')
            ->paginate($request->get('count'));
        if ($tasksWithoutFilter->total() === 0) {
            $errorMessage = '<a href="/#/task-category/list"><b>'.Translator::translate('Категории и задачи').'</b></a>';
            throw new HttpException(404,Translator::translate('В выбранном разделе задач нет. Создайте их на странице ').$errorMessage);
        }
        if (!$request->get('filter')['user_id']) {
            throw new HttpException(404, 'Выберите клиента');
        }
        $tasks = QueryBuilder::for(Task::class)
            ->allowedFilters([
                AllowedFilter::callback('task_category_id', static function (Builder $query, $value) {
                    return $query->where('task_category_id', '=',$value);
                }),
                AllowedFilter::callback('task_type', static function (Builder $query, $value) {
                    return $query->where('type', '=',$value);
                }),
                AllowedFilter::callback('only_assigned', static function (Builder $query, $value) use (&$request) {
                    if ($value === true) {
                        $query
                            ->whereDoesntHave('answers', function ($query) use (&$request) {
                                $query->where('user_task.user_id', '=', $request->get('filter')['user_id']);
                            });
                    }
                }),
                AllowedFilter::callback('user_id', static function (Builder $query, $value) {
                    return $query
                        ->with(['clients' => function($query) use ($value) {
                            $query->where('user_task.user_id', '=', $value);
                        }]);
                }),
                AllowedFilter::callback('difficult_level', static function (Builder $query, $value) {
                    return $query->where('difficult_level', '=',$value);
                }),
            ])
            ->orderBy('id')
            ->paginate($request->get('count'));
        $count = $tasksWithoutFilter->total() - $tasks->total();
        if ($request->get('filter')['only_assigned'] == "true" and $tasks->total() === 0) {
            if (!($request->get('filter')['difficult_level'] || $request->get('filter')['task_type'])) {
                throw new HttpException(422, Translator::translate('Все задачи этого раздела уже были назначены этому клиенту'));
            }
            $message = Translator::translate('Нет задач, соответствующих критерия выбора');
        }
        if ($request->get('filter')['only_assigned'] == "true" and $tasks->total() > 0 and $count > 0) {
            $message = Translator::translate('Кроме показанных задач, есть еще :count :task, ранее :assigned клиенту', [
                'count' => $count,
                'task' => Lang::choice(Translator::translate('задача').'|'.Translator::translate('задачи').'|'.Translator::translate('задач'), $count, [], 'ru'),
                'assigned' => Lang::choice(Translator::translate('назначенная').'|'.Translator::translate('назначенные').'|'.Translator::translate('назначенных'), $count, [], 'ru')
            ]);
        }
        if ($tasks->total() === 0) {
            $message = Translator::translate('Нет задач, соответствующих критерия выбора');
        }
        return [
            'tasks' => $tasks,
            'message' => $message
        ];
    }

    /** Список всех задач клиента */
    public static function buildQueryForTeacherClientTaskList(Request $request): LengthAwarePaginator
    {
        $userId = $request->get('filter')['user_id'];
        $query = Task::query()->withUserTask($userId);
        return QueryBuilder::for($query)
            ->allowedFilters([
                AllowedFilter::callback('task_category_id', static function (Builder $query, $value) {
                    return $query->where('task_category_id', '=',$value);
                }),
                AllowedFilter::callback('user_id', static function (Builder $query, $value) {
                    return $query
                        ->withWhereHas('clients', function($query) use ($value) {
                            $query->where('user_task.user_id', '=', $value);
                        });
                }),
                AllowedFilter::callback('assigned', fn(Builder $query, $value) =>
                    $query
                        ->withWhereHas('answer',fn($query) =>
                            $query
                                ->whereIn('answer.status', ['assigned', 'reassigned'])
                                ->where('user_task.user_id', '=', $userId))
                ),
                AllowedFilter::callback('in_review', fn(Builder $query, $value) =>
                    $query->withWhereHas('answer', fn($query) =>
                        $query->where('answer.status', '=', 'in_review')
                            ->where('user_task.user_id', '=', $userId)
                    )
                ),
                AllowedFilter::callback('all', fn(Builder $query, $value) =>
                    $query
                        ->withWhereHas('answer', fn ($query) =>
                            $query->where('user_task.user_id', '=', $userId)
                        )
                    ),
                AllowedFilter::callback('difficult_level', static function (Builder $query, $value) {
                    return $query->where('difficult_level', '=',$value);
                }),
            ])
            ->select('task.*')
            ->leftJoin('user_task','user_task.task_id','=','task.id')
            ->leftJoin('answer','answer.user_task_id','=','user_task.id')
            ->where('user_task.user_id', '=', $userId)
            ->orderBy('answer.created_at', 'desc')
            ->paginate($request->get('count'));
    }

    /** Все задачи только с назначениями */
    public static function taskListAll(Request $request): LengthAwarePaginator
    {
        return QueryBuilder::for(Task::class)
            ->withCount('answers')
            ->allowedFilters([
                AllowedFilter::callback('task_category_id', static function (Builder $query, $value) {
                    return $query->where('task_category_id', '=',$value);
                }),
                AllowedFilter::callback('task_type', static function (Builder $query, $value) {
                    return $query->where('type', '=',$value);
                }),
                AllowedFilter::callback('user_id', static function (Builder $query, $value) {
                    return $query
                        ->with('clients')
                        ->whereHas(
                            'clients', function($query) use ($value) {
                            $query->where('user_task.user_id', '=', $value);
                        });
                }),
                AllowedFilter::callback('assigned', fn(Builder $query, $value) =>
                    $query
                        ->with('answer')
                        ->whereHas('answer', fn($query) =>
                            $query->whereIn('answer.status', ['assigned', 'reassigned'])
                        )
                ),
                AllowedFilter::callback('in_review', fn(Builder $query, $value) =>
                    $query->with('answer')
                        ->whereHas('answer', fn($query) =>
                            $query->where('answer.status', '=', 'in_review')
                        )
                ),
                AllowedFilter::callback('all', fn(Builder $query, $value) =>
                    $query
                ),
                AllowedFilter::callback('difficult_level', static function (Builder $query, $value) {
                    return $query->where('difficult_level', '=',$value);
                }),
            ])
            ->orderBy('id')
            ->paginate($request->get('count'));
    }
}
