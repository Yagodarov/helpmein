<?php

declare(strict_types=1);

namespace App\Domain\User\Request;

use Illuminate\Foundation\Http\FormRequest;

class UserProfileEditAvatarRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'image' => 'sometimes|nullable|mimes:jpg,jpeg,bmp,png|file|max:2048',
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('first_name'),
        ];
    }
}
